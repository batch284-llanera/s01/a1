Make a new file called solution.txt inside s01-a1 folder and answer the following questions based on the image:
1. List the books Authored by Marjorie Green.
2. List the books Authored by Michael O'Leary.
3. Write the author/s of "The Busy Executive’s Database Guide".
4. Identify the publisher of "But Is It User Friendly?".
5. List the books published by Algodata Infosystems.


Answers:
1.
- The Busy Executive's Database Guide
- You Can Combat Computer Stress
2. 
- Cooking with Computers
3.
- Marjorie Green
- Abraham Bennet
4. 
- Algodata Infosystems
5.
- The Busy Executive's Database Guide
- Cooking with Computers
- Straight Talk About Computers
- But Is It User Friendly?
- Secrets of Silicon Valley
- Net Etiquette
